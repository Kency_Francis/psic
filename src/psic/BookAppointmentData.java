/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kency Unni
 */
public class BookAppointmentData {

    private Date startTime;
    private Date endTime;
    private Date newDate = new Date();
    private String room;
    private int phy_unique_id;
    String phy_full_name;
    private String visitorName = null;
    private int patientID;
    private int bookingID;
    private String treatment;
    static int firstBook = 0;
    static int bid = 500;
    static Physician phy = new Physician();
    static Scanner scan = new Scanner(System.in);
    private boolean booked = false, attended = false, changed = false, cancelled = false, Visitor = false, missed=false;
    public static ArrayList<BookAppointmentData> appointment = new ArrayList<BookAppointmentData>();
    public static ArrayList<Patient> patient = new ArrayList<Patient>();

    public BookAppointmentData() {

    }

    public BookAppointmentData(Date sDate, Date eDate, String treat, String roomNo, String phyName, int id) {
        this.startTime = sDate;
        this.endTime = eDate;
        this.room = roomNo;
        this.treatment = treat;
        this.phy_full_name = phyName;
        this.phy_unique_id = id;

    }

    public BookAppointmentData(Date sDate, Date eDate, String treat, String roomNo, String phyName, int id, boolean isVis) {
        this.startTime = sDate;
        this.endTime = eDate;
        this.room = roomNo;
        this.treatment = treat;
        this.phy_full_name = phyName;
        this.phy_unique_id = id;
        this.Visitor = isVis;

    }

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the newDate
     */
    public Date getNewDate() {
        return newDate;
    }

    /**
     * @param newDate the newDate to set
     */
    public void setNewDate(Date newDate) {
        this.newDate = newDate;
    }

    @Override
    public String toString() {
        return "\n" +"Physician Name:" + phy_full_name + "  Physician ID:" + phy_unique_id + "  Start time :" + getStartTime() + "  End time :" + getEndTime() +"  Treatment : " + treatment + "Room  :" + room;
    }

    /**
     * @return the booked
     */
    public boolean isBooked() {
        return booked;
    }

    /**
     * @param booked the booked to set
     */
    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    /**
     * @return the attended
     */
    public boolean isAttended() {
        return attended;
    }

    /**
     * @param attended the attended to set
     */
    public void setAttended(boolean attended) {
        this.attended = attended;
    }

    /**
     * @return the changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * @param changed the changed to set
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    /**
     * @return the cancelled
     */
    public boolean isCancelled() {
        return cancelled;
    }

    /**
     * @param cancelled the cancelled to set
     */
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    /**
     * @return the isVisitor
     */
    public boolean isVisitor() {
        return Visitor;
    }

    /**
     * @param Visitor the isVisitor to set
     */
    public void setVisitor(boolean Visitor) {
        this.Visitor = Visitor;
    }

    /**
     * @return the patientID
     */
    public int getPatientID() {
        return patientID;
    }

    /**
     * @param patientID the patientID to set
     */
    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    /**
     * @return the bookingID
     */
    public int getBookingID() {
        return bookingID;
    }

    /**
     * @param bookingID the bookingID to set
     */
    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public static Date dateGenerator(String Datetime) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+1"));
        Date result = null;
        try {
            result = formatter.parse(Datetime);
        } catch (ParseException ex) {
            Logger.getLogger(BookAppointmentData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static void addAppointment() {

        appointment.add(new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("03-06-2021 14:00:00"), dateGenerator("03-06-2021 15:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("04-06-2021 16:00:00"), dateGenerator("04-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("05-06-2021 17:00:00"), dateGenerator("05-06-2021 18:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:00:00"), dateGenerator("06-06-2021 18:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:30:00"), dateGenerator("06-06-2021 19:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:00:00"), dateGenerator("06-06-2021 19:30:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:30:00"), dateGenerator("06-06-2021 20:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1, true));

        appointment.add(new BookAppointmentData(dateGenerator("12-06-2021 13:00:00"), dateGenerator("12-06-2021 14:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("13-06-2021 14:00:00"), dateGenerator("13-06-2021 15:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("14-06-2021 16:00:00"), dateGenerator("14-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("15-06-2021 17:00:00"), dateGenerator("15-06-2021 18:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:00:00"), dateGenerator("16-06-2021 18:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:30:00"), dateGenerator("16-06-2021 19:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:00:00"), dateGenerator("16-06-2021 19:30:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:30:00"), dateGenerator("16-06-2021 20:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1, true));

        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 13:00:00"), dateGenerator("17-06-2021 14:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("18-06-2021 14:00:00"), dateGenerator("18-06-2021 15:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("19-06-2021 16:00:00"), dateGenerator("19-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("20-06-2021 17:00:00"), dateGenerator("20-06-2021 18:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 18:00:00"), dateGenerator("21-06-2021 18:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 18:30:00"), dateGenerator("21-06-2021 19:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 19:00:00"), dateGenerator("21-06-2021 19:30:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 19:30:00"), dateGenerator("21-06-2021 20:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1, true));

        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 13:00:00"), dateGenerator("22-06-2021 14:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 14:00:00"), dateGenerator("23-06-2021 15:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("24-06-2021 16:00:00"), dateGenerator("24-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("25-06-2021 17:00:00"), dateGenerator("25-06-2021 18:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 18:00:00"), dateGenerator("26-06-2021 18:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 18:30:00"), dateGenerator("26-06-2021 19:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 19:00:00"), dateGenerator("26-06-2021 19:30:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 19:30:00"), dateGenerator("26-06-2021 20:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1, true));

        appointment.add(new BookAppointmentData(dateGenerator("27-06-2021 13:00:00"), dateGenerator("27-06-2021 14:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 14:00:00"), dateGenerator("28-06-2021 15:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("29-06-2021 16:00:00"), dateGenerator("29-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("30-06-2021 17:00:00"), dateGenerator("30-06-2021 18:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1));
        appointment.add(new BookAppointmentData(dateGenerator("31-06-2021 18:00:00"), dateGenerator("31-06-2021 18:30:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("31-06-2021 18:30:00"), dateGenerator("31-06-2021 19:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("31-06-2021 19:00:00"), dateGenerator("31-06-2021 19:30:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite A", "Dr. Abel", 1, true));
        appointment.add(new BookAppointmentData(dateGenerator("31-06-2021 19:30:00"), dateGenerator("31-06-2021 20:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite A", "Dr. Abel", 1, true));

        appointment.add(new BookAppointmentData(dateGenerator("03-06-2021 13:00:00"), dateGenerator("03-06-2021 14:00:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("04-06-2021 14:00:00"), dateGenerator("04-06-2021 15:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("05-06-2021 15:00:00"), dateGenerator("05-06-2021 16:00:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 16:00:00"), dateGenerator("06-06-2021 17:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("07-06-2021 17:00:00"), dateGenerator("07-06-2021 18:00:00"), "Craniosacral                  ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:00:00"), dateGenerator("06-06-2021 18:30:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:30:00"), dateGenerator("06-06-2021 19:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:00:00"), dateGenerator("06-06-2021 19:30:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:30:00"), dateGenerator("06-06-2021 20:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2, true));

        appointment.add(new BookAppointmentData(dateGenerator("13-06-2021 13:00:00"), dateGenerator("13-06-2021 14:00:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("14-06-2021 14:00:00"), dateGenerator("14-06-2021 15:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("15-06-2021 15:00:00"), dateGenerator("15-06-2021 16:00:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 16:00:00"), dateGenerator("16-06-2021 17:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 17:00:00"), dateGenerator("17-06-2021 18:00:00"), "Craniosacral                  ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:00:00"), dateGenerator("16-06-2021 18:30:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:30:00"), dateGenerator("16-06-2021 19:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:00:00"), dateGenerator("16-06-2021 19:30:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:30:00"), dateGenerator("16-06-2021 20:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2, true));

        appointment.add(new BookAppointmentData(dateGenerator("18-06-2021 13:00:00"), dateGenerator("18-06-2021 14:00:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("19-06-2021 14:00:00"), dateGenerator("19-06-2021 15:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("20-06-2021 15:00:00"), dateGenerator("20-06-2021 16:00:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 16:00:00"), dateGenerator("21-06-2021 17:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 17:00:00"), dateGenerator("22-06-2021 18:00:00"), "Craniosacral                  ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 18:00:00"), dateGenerator("23-06-2021 18:30:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 18:30:00"), dateGenerator("23-06-2021 19:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 19:00:00"), dateGenerator("23-06-2021 19:30:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 19:30:00"), dateGenerator("23-06-2021 20:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2, true));

        appointment.add(new BookAppointmentData(dateGenerator("24-06-2021 13:00:00"), dateGenerator("24-06-2021 14:00:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("25-06-2021 14:00:00"), dateGenerator("25-06-2021 15:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 15:00:00"), dateGenerator("26-06-2021 16:00:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("27-06-2021 16:00:00"), dateGenerator("27-06-2021 17:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 17:00:00"), dateGenerator("28-06-2021 18:00:00"), "Craniosacral                  ", "Medical consulting suite B", "Dr. Angel", 2));
        appointment.add(new BookAppointmentData(dateGenerator("29-06-2021 18:00:00"), dateGenerator("29-06-2021 18:30:00"), "Muscle Energy                 ", "Gym", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("29-06-2021 18:30:00"), dateGenerator("29-06-2021 19:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("29-06-2021 19:00:00"), dateGenerator("29-06-2021 19:30:00"), "Myofascial Release            ", "Medical consulting suite B", "Dr. Angel", 2, true));
        appointment.add(new BookAppointmentData(dateGenerator("29-06-2021 19:30:00"), dateGenerator("29-06-2021 20:00:00"), "Counterstrain                 ", "Medical consulting suite B", "Dr. Angel", 2, true));

        appointment.add(new BookAppointmentData(dateGenerator("03-06-2021 13:00:00"), dateGenerator("03-06-2021 14:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("04-06-2021 14:00:00"), dateGenerator("04-06-2021 15:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("05-06-2021 15:00:00"), dateGenerator("05-06-2021 16:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 16:00:00"), dateGenerator("06-06-2021 17:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("07-06-2021 17:00:00"), dateGenerator("07-06-2021 18:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:00:00"), dateGenerator("06-06-2021 18:30:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:30:00"), dateGenerator("06-06-2021 19:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:00:00"), dateGenerator("06-06-2021 19:30:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:30:00"), dateGenerator("06-06-2021 20:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));

        appointment.add(new BookAppointmentData(dateGenerator("13-06-2021 13:00:00"), dateGenerator("13-06-2020 14:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("14-06-2021 14:00:00"), dateGenerator("14-06-2020 15:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("15-06-2021 15:00:00"), dateGenerator("15-06-2020 16:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 16:00:00"), dateGenerator("16-06-2020 17:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 17:00:00"), dateGenerator("17-06-2020 18:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:00:00"), dateGenerator("16-06-2020 18:30:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:30:00"), dateGenerator("16-06-2020 19:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:00:00"), dateGenerator("16-06-2020 19:30:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:30:00"), dateGenerator("16-06-2020 20:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));

        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 13:00:00"), dateGenerator("17-06-2020 14:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("18-06-2021 14:00:00"), dateGenerator("18-06-2020 15:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("19-06-2021 15:00:00"), dateGenerator("19-06-2020 16:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("19-06-2021 15:00:00"), dateGenerator("19-06-2020 16:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("20-06-2021 16:00:00"), dateGenerator("20-06-2020 17:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 17:00:00"), dateGenerator("21-06-2020 18:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 18:00:00"), dateGenerator("22-06-2020 18:30:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 18:30:00"), dateGenerator("22-06-2020 19:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 19:00:00"), dateGenerator("22-06-2020 19:30:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 19:30:00"), dateGenerator("22-06-2020 20:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));

        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 13:00:00"), dateGenerator("23-06-2020 14:00:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("24-06-2021 14:00:00"), dateGenerator("24-06-2020 15:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("25-06-2021 15:00:00"), dateGenerator("25-06-2020 16:00:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 16:00:00"), dateGenerator("26-06-2020 17:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("27-06-2021 17:00:00"), dateGenerator("27-06-2020 18:00:00"), "Lymphatic Pump Treatment      ", "Medical consulting suite C", "Dr. Lekshmi", 3));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 18:00:00"), dateGenerator("28-06-2020 18:30:00"), "Cardiovascular strengthening  ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 18:30:00"), dateGenerator("28-06-2020 19:00:00"), "Muscle Energy                 ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 19:00:00"), dateGenerator("28-06-2020 19:30:00"), "Cryotherapy and Heat Thearapy ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 19:30:00"), dateGenerator("28-06-2020 20:00:00"), "Speech Therapy                ", "Medical consulting suite C", "Dr. Lekshmi", 3, true));

        appointment.add(new BookAppointmentData(dateGenerator("01-06-2021 13:00:00"), dateGenerator("13-06-2020 14:00:00"), "Relaxation Therapy            ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("02-06-2021 14:00:00"), dateGenerator("14-06-2020 15:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 15:00:00"), dateGenerator("15-06-2020 16:00:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("18-06-2021 16:00:00"), dateGenerator("18-06-2020 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:00:00"), dateGenerator("16-06-2020 18:30:00"), "Counsling                     ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:30:00"), dateGenerator("16-06-2020 19:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:00:00"), dateGenerator("16-06-2020 19:30:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:30:00"), dateGenerator("16-06-2021 20:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4, true));

        appointment.add(new BookAppointmentData(dateGenerator("13-06-2021 13:00:00"), dateGenerator("13-06-2021 14:00:00"), "Relaxation Therapy            ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 14:00:00"), dateGenerator("14-06-2021 15:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 15:00:00"), dateGenerator("15-06-2021 16:00:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("18-06-2021 16:00:00"), dateGenerator("18-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:00:00"), dateGenerator("16-06-2021 18:30:00"), "Counsling                     ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:30:00"), dateGenerator("16-06-2021 19:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:00:00"), dateGenerator("16-06-2021 19:30:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:30:00"), dateGenerator("16-06-2021 20:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4, true));

        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 13:00:00"), dateGenerator("17-06-2021 14:00:00"), "Relaxation Therapy            ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("18-06-2021 14:00:00"), dateGenerator("18-06-2021 15:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("19-06-2021 15:00:00"), dateGenerator("19-06-2021 16:00:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("20-06-2021 16:00:00"), dateGenerator("20-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 18:00:00"), dateGenerator("21-06-2021 18:30:00"), "Counsling                     ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 18:30:00"), dateGenerator("21-06-2021 19:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 19:00:00"), dateGenerator("22-06-2021 19:30:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 19:30:00"), dateGenerator("22-06-2021 20:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4, true));

        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 13:00:00"), dateGenerator("23-06-2021 14:00:00"), "Relaxation Therapy            ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 14:00:00"), dateGenerator("26-06-2021 15:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("27-06-2021 15:00:00"), dateGenerator("27-06-2021 16:00:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 16:00:00"), dateGenerator("28-06-2021 17:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 18:00:00"), dateGenerator("26-06-2021 18:30:00"), "Counsling                     ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 18:30:00"), dateGenerator("26-06-2021 19:00:00"), "Psychotherapy                 ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("27-06-2021 19:00:00"), dateGenerator("26-06-2021 19:30:00"), "Interpersonal Therapy         ", "Medical consulting suite D", "Dr. Ajith", 4, true));
        appointment.add(new BookAppointmentData(dateGenerator("27-06-2021 19:30:00"), dateGenerator("26-06-2021 20:00:00"), "Soft Tissue Mobilisation      ", "Medical consulting suite D", "Dr. Ajith", 4, true));

        appointment.add(new BookAppointmentData(dateGenerator("03-06-2021 13:00:00"), dateGenerator("03-06-2021 14:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("04-06-2021 14:00:00"), dateGenerator("04-06-2021 15:00:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("05-06-2021 15:00:00"), dateGenerator("05-06-2021 16:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 16:00:00"), dateGenerator("06-06-2021 17:00:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("07-06-2021 16:00:00"), dateGenerator("07-06-2021 17:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:00:00"), dateGenerator("06-06-2021 18:30:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 18:30:00"), dateGenerator("06-06-2021 19:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:00:00"), dateGenerator("06-06-2021 19:30:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("06-06-2021 19:30:00"), dateGenerator("06-06-2021 20:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));

        appointment.add(new BookAppointmentData(dateGenerator("13-06-2021 13:00:00"), dateGenerator("13-06-2021 14:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("14-06-2021 14:00:00"), dateGenerator("14-06-2021 15:00:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("15-06-2021 15:00:00"), dateGenerator("15-06-2021 16:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 16:00:00"), dateGenerator("16-06-2021 17:00:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 16:00:00"), dateGenerator("17-06-2021 17:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:00:00"), dateGenerator("16-06-2021 18:30:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 18:30:00"), dateGenerator("16-06-2021 19:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:00:00"), dateGenerator("16-06-2021 19:30:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("16-06-2021 19:30:00"), dateGenerator("16-06-2021 20:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));

        appointment.add(new BookAppointmentData(dateGenerator("17-06-2021 13:00:00"), dateGenerator("17-06-2021 14:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("18-06-2021 14:00:00"), dateGenerator("18-06-2021 15:00:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("19-06-2021 15:00:00"), dateGenerator("19-06-2021 16:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("20-06-2021 16:00:00"), dateGenerator("20-06-2021 17:00:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("21-06-2021 16:00:00"), dateGenerator("21-06-2021 17:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 18:00:00"), dateGenerator("22-06-2021 18:30:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("22-06-2021 18:30:00"), dateGenerator("22-06-2021 19:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 19:00:00"), dateGenerator("23-06-2021 19:30:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("23-06-2021 19:30:00"), dateGenerator("23-06-2021 20:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));

        appointment.add(new BookAppointmentData(dateGenerator("24-06-2021 13:00:00"), dateGenerator("24-06-2021 14:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("25-06-2021 14:00:00"), dateGenerator("25-06-2021 15:00:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("26-06-2021 15:00:00"), dateGenerator("26-06-2021 16:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("27-06-2021 16:00:00"), dateGenerator("27-06-2021 17:00:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5));
        appointment.add(new BookAppointmentData(dateGenerator("28-06-2021 16:00:00"), dateGenerator("28-06-2021 17:00:00"), "Diabetic Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("29-06-2021 18:00:00"), dateGenerator("29-06-2021 18:30:00"), "Artritis Control              ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("29-06-2021 18:30:00"), dateGenerator("29-06-2021 19:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("30-06-2021 19:00:00"), dateGenerator("30-06-2021 19:30:00"), "Weight Gaining                ", "Medical consulting suite E", "Dr. Robinson", 5, true));
        appointment.add(new BookAppointmentData(dateGenerator("30-06-2021 19:30:00"), dateGenerator("30-06-2021 20:00:00"), "Weight Loss                   ", "Medical consulting suite E", "Dr. Robinson", 5, true));
    }

    /**
     * @return the missed
     */
    public boolean isMissed() {
        return missed;
    }

    /**
     * @param missed the missed to set
     */
    public void setMissed(boolean missed) {
        this.missed = missed;
    }

}
