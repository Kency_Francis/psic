/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Kency Unni
 */
public class Patient extends Person {

    private Scanner scan = new Scanner(System.in);
    public static ArrayList<Patient> registeredPatient = new ArrayList<>();
//    private ArrayList<Patient> patient = new ArrayList<>();

    public Patient() {
        super();

    }

    public Patient(int id, String fname, String addr, String ph) {
        super(id, fname, addr, ph);
    }

    public int getUnique_id() {
        return this.unique_id;
    }

    public String getPatientName() {
        return this.full_name;
    }

    @Override
    public String toString() {
        return getUnique_id() + "   " + full_name + "   " + address + "   " + telephone;
    }

    public boolean physicianName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void getNewPatient(int uniqueID) {

        System.out.println("Please enter patient's name");
        full_name = scan.nextLine();
        System.out.println("Please enter patient's address");
        address = scan.nextLine();
        System.out.println("Please enter patient's mobile number");
        telephone = scan.nextLine();
        registeredPatient.add(new Patient(uniqueID, full_name, address, telephone));
        System.out.println("Thank you !! Patient Details Registered!!!");
    }

    public void listPatientUsingPID(int pID) {
        int count = 0;
        System.out.println("**************************************************");
        System.out.println("PATIENT DETAILS ");
        System.out.println("**************************************************");
        System.out.println("**************************************************");
        for (Patient pat : registeredPatient) {
            if (pat.unique_id == pID) {
                System.out.println("ID" + "   " + "Patient Name" + "   " + "Address" + "   " + "Phone Number");
                System.out.println(pat.toString());
                count++;
                break;
            }
        }
        if (count == 0) {
            System.out.println("Wrong Patient ID/Entered Patient is not registered in the system!!");
        }
    }

    public void listAllPatient() {
        System.out.println("**************************************************");
        System.out.println("LISTING ALL REGISTERED PATIENT DETAILS ");
        System.out.println("**************************************************");
        System.out.println("**************************************************");
        System.out.println("ID" + "   " + "Patient Name" + "   " + "Address" + "   " + "Phone Number");
        for (Patient pat : registeredPatient) {
            System.out.println(pat.toString());
        }
    }

    public void preRegisteredPatient() {
        registeredPatient.add(new Patient(121, "Kency Francis   ", "LU1 2FQ", "0756367237"));
        registeredPatient.add(new Patient(122, "Kevin Francis   ", "LU2 2ED", "0757767681"));
        registeredPatient.add(new Patient(123, "Kenson Francis  ", "LU3 2TQ", "0757767682"));
        registeredPatient.add(new Patient(124, "Francis Stephen ", "LU4 2YH", "0757767683"));
        registeredPatient.add(new Patient(125, "Mary Jose       ", "LU5 2CD", "0757767684"));
        registeredPatient.add(new Patient(226, "Sible Annie     ", "LU6 2SG", "0777676850"));
        registeredPatient.add(new Patient(227, "Robinson Joe    ", "LU7 5GF", "0757676868"));
        registeredPatient.add(new Patient(228, "Sona Reji       ", "LU8 7SS", "0757676876"));
        registeredPatient.add(new Patient(229, "Allen Johnson   ", "LU9 7VF", "0757776884"));
        registeredPatient.add(new Patient(303, "Issac Stephen   ", "AB1 9VF", "0757766894"));
        registeredPatient.add(new Patient(313, "Alex Safin      ", "AB2 1DF", "0757767603"));
        registeredPatient.add(new Patient(323, "Lilly Shyni     ", "AB3 4FR", "0757767611"));
        registeredPatient.add(new Patient(333, "Stella Stephen  ", "AB4 7VD", "0757767629"));
        registeredPatient.add(new Patient(343, "Sereena Willam  ", "AB5 5FD", "0757767633"));
        registeredPatient.add(new Patient(353, "Rooney Libson   ", "AB6 2YQ", "0757767614"));
    }

    public static String getPatientNameUsingPID(int patID) {
        for (Patient p : registeredPatient) {
            if (p.getUnique_id() == patID) {
                return p.getPatientName();
            }
        }
        return null;
    }

    public static int getPIDNameUsingPname(String pname) {
        for (Patient p : registeredPatient) {
            if (p.getPatientName().equals(pname)) {
                return p.getUnique_id();
            }
        }
        return 0;
    }

    public void setUnique_id(int unique_id) {
        this.unique_id = unique_id;
    }

    /**
     *
     * @param uniqueID
     */
    public void newPatientGet(int uniqueID) {
        // TODO - implement Patient.newPatientGet
        throw new UnsupportedOperationException();
    }
}
