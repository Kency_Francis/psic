/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Kency Unni
 */
public class PSICMain {

    /**
     * @param args the command line arguments
     */
    Scanner scan;
    private ArrayList<Physician> p;
    private static Patient pat = new Patient();
    private static Physician phy = new Physician();
    private static BookAppointmentData app = new BookAppointmentData();
    private static Report rep = new Report();

    public static void main(String[] args) throws ArrayIndexOutOfBoundsException, IOException {
        // TODO code application logic here
        int loop_terminate=0;
        boolean loop_terminate1 = true;
        int uniqueID = 100;
        int choice = 0, choice1 = 0;
        PSICMain psic = new PSICMain();
        psic.scan = new Scanner(System.in);
        pat.preRegisteredPatient();
        BookAppointmentData.addAppointment();
        Physician.addPhysician();
        System.out.println("Physiotherapy & Sports Injury Centre (PSIC) ");
        while (loop_terminate1) {
            System.out.println("**************************************************");
            System.out.println("!!! MAIN MENU !!!");
            System.out.println("Make your choice");
            System.out.println("Press 1 for registration- New Patient/Existing Patient");
            System.out.println("Press 2 to view patient details");
            System.out.println("Press 3 to book a treatment appoinment ");
            System.out.println("Press 4 to change/cancel a booking");
            System.out.println("Press 5 to attend a treatment appoinment");
            System.out.println("Press 6 to book a visitor appoinment");
            System.out.println("Press 7 to print the term report-1");
            System.out.println("Press 8 to print the term report-2");
            System.out.println("Press 9 to print ReportALL");
            System.out.println("Press 0 to EXIT");
            System.out.println("**************************************************");
            try {
                choice = Integer.parseInt(psic.scan.nextLine()); //When scan.nextInt() is used, patient name is being skipped
            } catch (NumberFormatException e) {
                System.out.println(" Invalid input");
            }
            switch (choice) {
                case 1:
                    int num = 0;
                    System.out.println("**************************************************");
                    System.out.println("New Patient Registration");
                    System.out.println("**************************************************");
                    uniqueID = uniqueID + 1;
                    pat.getNewPatient(uniqueID);
                    System.out.println("Please note your PatientID for booking your appoinments");
                    System.out.println("Your PatientID is : " + uniqueID);
                    System.out.println("**************************************************");
                    break;
                case 2:
                    int num1 = 0;
                    System.out.println("**************************************************");
                    System.out.println("View Patient Details");
                    System.out.println("**************************************************");
                    System.out.println("**************************************************");
                    System.out.println("Book a treatment appoinment");
                    System.out.println("Press 1 to view patient details using patient ID ");
                    System.out.println("Press 2 to view all the patients registered in the system");
                    System.out.println("**************************************************");
                    try {
                        num1 = Integer.parseInt(psic.scan.nextLine());
                    } catch (NumberFormatException e) {
                        System.out.println(" Invalid input");
                    }
                    switch (num1) {
                        case 1:
                            int patid = 0;
                            System.out.println("Please enter your patient ID");
                            try {
                                patid = Integer.parseInt(psic.scan.nextLine());
                            } catch (NumberFormatException e) {
                                System.out.println(" Invalid input");
                            }

                            pat.listPatientUsingPID(patid);
                            break;
                        case 2:
                            pat.listAllPatient();
                            break;
                        default:
                            System.out.println("Invalid Option");
                            break;
                    }
                    System.out.println("**************************************************");
                    break;
                case 3:
                    System.out.println("**************************************************");
                    System.out.println("Book treatment appointment");
                    System.out.println("**************************************************");
                    phy.physicianLookUp(0);
                    break;
                case 4:
                    System.out.println("**************************************************");
                    System.out.println("Change/Cancel an booked appoinment");
                    System.out.println("Press 1 to change appointment");
                    System.out.println("Press 2 to cancel appointment");
                    try {
                        choice1 = Integer.parseInt(psic.scan.nextLine());
                    } catch (NumberFormatException e) {
                        System.out.println(" Invalid input");
                    }
                    switch (choice1) {
                        case 1:
                            System.out.println("**************************************************");
                            System.out.println("Change Appointment");
                            BookAppointment.changeAppointment();
                            System.out.println("**************************************************");
                            break;
                        case 2:
                            System.out.println("**************************************************");
                            System.out.println("Cancel Appointment");
                            BookAppointment.cancelAppointment();
                            break;
                        default:
                            System.out.println("Invalid Option");
                            break;
                    }
                    System.out.println("**************************************************");
                    break;
                case 5:
                    int patientID = 0;
                    System.out.println("**************************************************");
                    System.out.println("Mark an attended appointment");
                    System.out.println("Please enter the patient id");
                    try {
                        patientID = Integer.parseInt(psic.scan.nextLine());
                    } catch (NumberFormatException e) {
                        System.out.println(" Invalid input");
                    }
                    BookAppointment.attend(patientID);
                    System.out.println("**************************************************");
                    break;
                case 6:
                    System.out.println("**************************************************");
                    System.out.println("Book Visitor Appointment");
                    phy.physicianLookUp(1);
                    System.out.println("**************************************************");
                    break;
                case 7:
                    Report.report1();
                    break;
                case 8:
                    Report.reportt2();
                    break;
                case 9:
                    Report.reportALL();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("INVALID OPTION");
                    break;
            }

            System.out.println("Do you want to continue?? (press 1 to continue)");
            try {
                loop_terminate = Integer.parseInt(psic.scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println(" Invalid input");
            }
            if (loop_terminate != 1) {
                loop_terminate1 = false;
            } else {
                loop_terminate1 = true;
            }
        }

    }

}
