/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

/**
 *
 * @author Kency Unni
 */
public class Person {
    public int unique_id;
    public String full_name;
    public String address;
    public String telephone;
    
    public Person(){
        
    }
    
    public Person(int uid, String full_name1, String address1, String telephone1) {
        this.unique_id = uid;
        this.full_name = full_name1;
        this.address = address1;
        this.telephone = telephone1;
        
    }

}
