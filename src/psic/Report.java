/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Kency Unni
 */
public class Report {

    public static void report1() {
        System.out.println("**************************************************");
        System.out.println("                    REPORT 1                      ");
        System.out.println("**************************************************");
        int count = 0;
        int count1 = 0;
        String patName;
        System.out.println("**************************************************");
        System.out.println("---------Listing All Patient Appointments----------");
        for (BookAppointmentData app : BookAppointmentData.appointment) {
            if (app.isBooked() == true && app.isVisitor() == false) {
                patName = Patient.getPatientNameUsingPID(app.getPatientID());
                System.out.println("Patient Name : " + patName + " " + "Patient unique_id: " + app.getPatientID() + "   " + app.toString());
                count1++;
            }
        }
        if (count1 == 0) {
            System.out.println("No Appointments Booked for Patients!!!");
        }
        System.out.println("---------Listing All Visitor Appointments---------");
        for (BookAppointmentData appo : BookAppointmentData.appointment) {
            if (appo.isVisitor() == true && appo.isBooked() == true) {
                System.out.print("Visitor Name :  " + "   " + appo.getVisitorName());
                System.out.println(appo.toString());
                count++;
            }
        }
        if (count == 0) {
            System.out.println("No Appointments Booked for Visitors!!!");
        }
        System.out.println("**************************************************");
    }

    public static void reportt2() {
        Map<Integer, List<BookAppointmentData>> patientReportMap = new HashMap<>();
        System.out.println("**************************************************");
        System.out.println("                    REPORT 2                      ");
        System.out.println("**************************************************");
        System.out.println("----------Listing All Appointments Details--------");
        System.out.println("**************************************************");
        for (BookAppointmentData a : BookAppointmentData.appointment) {
            if (!patientReportMap.containsKey(a.getPatientID())) {
                patientReportMap.put(a.getPatientID(), new ArrayList<>());
            }
            patientReportMap.get(a.getPatientID()).add(a);
        }
        for (Map.Entry reportMap : patientReportMap.entrySet()) {
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            int count4 = 0;
            int count5 = 0;
            int pID = (int) reportMap.getKey();
            ArrayList a = (ArrayList) reportMap.getValue();
            String patientName = Patient.getPatientNameUsingPID(pID);
            int ppID = Patient.getPIDNameUsingPname(patientName);
            if (patientName != null) {
                System.out.println("**************************************************");
                System.out.println("..................................................");
                System.out.println("Patient Name : " + patientName + "Patient ID : " + ppID);
                System.out.println("...........Listing booking details of " + patientName);
                for (Object app : a) {
                    BookAppointmentData app1 = (BookAppointmentData) app;
                    if (app1.isBooked() == true) {
                        System.out.println("Booked : " + app1.toString());
                        count1++;
                    }
                    if (app1.isCancelled() == true) {
                        System.out.println("Cancelled : " + app1.toString());
                        count2++;
                    }
                    if (app1.isAttended() == true) {
                        System.out.println("Attended : " + app1.toString());
                        count3++;
                    }
                    if (app1.isChanged() == true) {
                        System.out.println("Changed : " + app1.toString());
                        count4++;
                    }
                    if (app1.isMissed() == true) {
                        System.out.println("Missed : " + app1.toString());
                        count5++;
                    }
                }
                System.out.println("...........Lisiting the count");
                System.out.println("Booked count: " + count1 + "  Cancelled count: " + count2 + "  Attended count:  " + count3 + "  Changed count:  " + count4 + " Missed count: "+ count5);
                System.out.println("..................................................");
            }
        }
        if (patientReportMap.size() == 1) {
            System.out.println("No appointments Booked!!");
        }
        System.out.println("**************************************************");
    }

    public static void reportALL() {
        String patientName;
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count5 = 0;
        System.out.println("**************************************************");
        System.out.println("                    REPORT ALL                      ");
        System.out.println("**************************************************");
        System.out.println("----------Listing All Appointments Booked---------");
        for (BookAppointmentData appoi : BookAppointmentData.appointment) {
            if (appoi.isBooked() == true && appoi.isChanged() == false && appoi.isVisitor() == false) {
                patientName = Patient.getPatientNameUsingPID(appoi.getPatientID());
                System.out.println("Patient Name : " + patientName + " " + "Patient unique_id: " + appoi.getPatientID() + "   " + appoi.toString());
            }
            if (appoi.isBooked() == true && appoi.isChanged() == false && appoi.isVisitor() == true) {
                patientName = appoi.getVisitorName();
                System.out.println("Visitor Name : " + patientName + " " + "   " + appoi.toString());
            }
        }
        if (count3 == 0) {
            {
                System.out.println(" No appointments Booked !!! ");
            }
        }
        System.out.println("----------Listing All Appoinments Attended--------");
        for (BookAppointmentData appoi : BookAppointmentData.appointment) {
            if (appoi.isAttended() == true && appoi.isCancelled() == false && appoi.isVisitor() == false) {
                patientName = Patient.getPatientNameUsingPID(appoi.getPatientID());
                System.out.println("Patient Name : " + patientName + " " + "Patient unique_id: " + appoi.getPatientID() + "   " + appoi.toString());
                count2++;
            }
            if (appoi.isAttended() == true && appoi.isCancelled() == false && appoi.isVisitor() == true) {
                patientName = appoi.getVisitorName();
                System.out.println("Visitor Name : " + patientName + " " + "   " + appoi.toString());
            }
        }
        if (count2 == 0) {
            System.out.println(" No Appoinments Attended !!! ");
        }
        System.out.println("-----------Listing All Cancelled Appointments-----");
        for (BookAppointmentData appoi : BookAppointmentData.appointment) {
            if (appoi.isCancelled() == true && appoi.isVisitor() == false) {
                patientName = Patient.getPatientNameUsingPID(appoi.getPatientID());
                count1++;
                System.out.println("Patient Name : " + patientName + " " + "Patient unique_id: " + appoi.getPatientID() + "   " + appoi.toString());
            }
            if (appoi.isCancelled() == true && appoi.isVisitor() == true) {
                patientName = appoi.getVisitorName();
                System.out.println("Visitor Name : " + patientName + " " + "   " + appoi.toString());
            }
        }
        if (count1 == 0) {
            System.out.println(" No Appointments Cancelled !!!");
        }
        System.out.println("----------Listing All Missed Appointments---------");
        for (BookAppointmentData appoi : BookAppointmentData.appointment) {
            int result = appoi.getStartTime().compareTo(appoi.getNewDate());
            if (appoi.isCancelled() == false && appoi.isAttended() == false && appoi.isVisitor() == false && appoi.isBooked() == true && result < 0) {
                patientName = Patient.getPatientNameUsingPID(appoi.getPatientID());
                count5++;
                System.out.println("Patient Name : " + patientName + " " + "Patient unique_id: " + appoi.getPatientID() + "   " + appoi.toString());
            }
            if (appoi.isCancelled() == false && appoi.isAttended() == false && appoi.isBooked() == true && result < 0 && appoi.isVisitor() == true) {
                patientName = appoi.getVisitorName();
                System.out.println("Visitor Name : " + patientName + " " + "   " + appoi.toString());
            }
        }
        if (count5 == 0) {
            System.out.println(" No Appoinments Missed !!!");
        }
    }

}
