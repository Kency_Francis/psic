/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Kency Unni
 */
public class Physician extends Person {

    public String[] expertise;
    public static ArrayList<Physician> phyList = new ArrayList<Physician>();
    private Scanner scan = new Scanner(System.in);

    public Physician() {
    }

    public Physician(int id, String fname, String addr, String ph, String[] expertise) {
        super(id, fname, addr, ph);
        this.expertise = expertise;
    }

    @Override
    public String toString() {
        return "" + unique_id + " " + full_name + " " + address + " " + telephone + " " + Arrays.toString(expertise);
    }

    public String getPhysicianName() {
        return full_name;
    }

    public int getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(int unique_id) {
        this.unique_id = unique_id;
    }

    public String[] getPhysicianExpertise() {
        return this.expertise;
    }

    public void listAllPhysicians() {
        for (Physician phy : phyList) {
            System.out.println(phy.toString());
        }
    }

    public static void addPhysician() {
        phyList.add(new Physician(1, "Dr. Abel", "LU3 2AB", "07126797213", new String[]{"Physiotherapy", "Osteopathy"}));
        phyList.add(new Physician(2, "Dr. Angel", "LU2 6AB", "07126678324", new String[]{"Osteopathy"}));
        phyList.add(new Physician(3, "Dr. Lekshmi", "LU2 7GB", "07737389892", new String[]{"Physiotherapy", "Rehabilitation", "Osteopathy"}));
        phyList.add(new Physician(4, "Dr. Ajith", "LU9 3AB", "07885537832", new String[]{"Psychologist", "Physiotherapy"}));
        phyList.add(new Physician(5, "Dr. Robinson", "LU9 3AB", "07885537832", new String[]{"Nutritionist",}));
    }

    public void physicianLookUp(int vis) throws FileNotFoundException, IOException {
        String visName = null;
        int pID = 0;
        if (vis == 1) {
            System.out.println("Please enter your name - Visitor");
            visName = scan.nextLine();
        } else {
            System.out.println("Entered patient ID");
            try {
                pID = Integer.parseInt(BookAppointmentData.scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println(" Invalid input");
            }
        }
        System.out.println("**************************************************");
        System.out.println("Book a treatment appoinment");
        System.out.println("Press 1 to list physician by name ");
        System.out.println("Press 2 to list physician by expertise ");
        System.out.println("**************************************************");
        int n = 0;
        try {
            n = Integer.parseInt(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println(" Invalid input");
        }
        switch (n) {
            case 1:
                System.out.println("Listing Physician by name");
                System.out.println("**************************************************");
                int increment = 1;
                HashMap<Integer, Physician> hmap = new HashMap<Integer, Physician>();
                for (Physician phy2 : phyList) {
                    System.out.println(increment + ". " + phy2.getPhysicianName());
                    hmap.put(increment, phy2);
                    increment++;
                }
                System.out.println("**************************************************");
                System.out.println("Please select the Physician you want to take appoinment");
                int nn = 0;
                try {
                    nn = Integer.parseInt(scan.nextLine());
                } catch (NumberFormatException e) {
                    System.out.println(" Invalid input");
                }
                Physician p = hmap.get(nn);
                if (vis == 1) {
                    BookAppointment.bookVisitorAppointment(p, visName);
                    break;
                } else {
                    BookAppointment.bookAppointment(p, pID);;
                    break;
                }
            case 2:
                int ch = 0;
                System.out.println("**************************************************");
                System.out.println("Listing Physician by expertise");
                System.out.println("Make your choice");
                System.out.println("Press 1 : Osteopathy ");
                System.out.println("Press 2 : Physiotherapy ");
                System.out.println("Press 3 : Rehabilitation ");
                System.out.println("Press 4 : Nutritionist ");
                System.out.println("Press 5 : Psychologist ");
                System.out.println("**************************************************");
                try {
                    ch = Integer.parseInt(scan.nextLine());
                } catch (NumberFormatException e) {
                    System.out.println(" Invalid input");
                }
                int opt = 0;
                switch (ch) {
                    case 1:
                        HashMap<Integer, Physician> hashmap = new HashMap<Integer, Physician>();
                        int count = 0;
                        for (Physician copy : phyList) {
                            if (Arrays.asList(copy.expertise).indexOf("Osteopathy") != -1) {
                                count++;
                                System.out.println("Press " + count + " for Physician: " + copy.getPhysicianName());
                                hashmap.put(count, copy);
                            }
                        }
                        try {
                            opt = Integer.parseInt(scan.nextLine());
                        } catch (NumberFormatException e) {
                            System.out.println(" Invalid input");
                        }
                        Physician phy = hashmap.get(opt);
                        if (vis == 1) {
                            BookAppointment.bookVisitorAppointment(phy, visName);
                        } else {
                            System.out.println("**************************************************");
                            System.out.println("Entered patient ID");
                            int patID = 0;
                            try {
                                patID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                            } catch (NumberFormatException e) {
                                System.out.println(" Invalid input");
                            }
                            BookAppointment.bookAppointment(phy, patID);
                        }
                        break;
                    case 2:
                        Map<Integer, Physician> hashmap1 = new HashMap<Integer, Physician>();
                        int count1 = 0;
                        for (Physician copy : phyList) {
                            if (Arrays.asList(copy.expertise).indexOf("Physiotherapy") != -1) {
                                count1++;
                                System.out.println("Press " + count1 + " for Physician: " + copy.getPhysicianName());
                                hashmap1.put(count1, copy);
                            }
                        }
                        try {
                            opt = Integer.parseInt(scan.nextLine());
                        } catch (NumberFormatException e) {
                            System.out.println(" Invalid input");
                        }
                        Physician phy1 = hashmap1.get(opt);
                        if (vis == 1) {
                            BookAppointment.bookVisitorAppointment(phy1, visName);
                        } else {
                            System.out.println("**************************************************");
                            System.out.println("Entered patient ID");
                            int patID = 0;
                            try {
                                patID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                            } catch (NumberFormatException e) {
                                System.out.println(" Invalid input");
                            }
                            BookAppointment.bookAppointment(phy1, patID);
                        }
                        break;
                    case 3:
                        Map<Integer, Physician> hashmap2 = new HashMap<Integer, Physician>();
                        int count2 = 0;
                        for (Physician copy : phyList) {
                            if (Arrays.asList(copy.expertise).indexOf("Rehabilitation") != -1) {
                                count2++;
                                System.out.println("Press " + count2 + " for Physician: " + copy.getPhysicianName());
                                hashmap2.put(count2, copy);
                            }
                        }
                        try {
                            opt = Integer.parseInt(scan.nextLine());
                        } catch (NumberFormatException e) {
                            System.out.println(" Invalid input");
                        }
                        Physician phy2 = hashmap2.get(opt);
                        if (vis == 1) {
                            BookAppointment.bookVisitorAppointment(phy2, visName);
                        } else {
                            System.out.println("**************************************************");
                            System.out.println("Entered patient ID");
                            int patID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                            BookAppointment.bookAppointment(phy2, patID);
                        }
                        break;
                    case 4:
                        Map<Integer, Physician> hashmap3 = new HashMap<Integer, Physician>();
                        int count3 = 0;
                        for (Physician copy : phyList) {
                            if (Arrays.asList(copy.expertise).indexOf("Nutritionist") != -1) {
                                count3++;
                                System.out.println("Press " + count3 + " for Physician: " + copy.getPhysicianName());
                                hashmap3.put(count3, copy);
                            }
                        }
                        try {
                            opt = Integer.parseInt(scan.nextLine());
                        } catch (NumberFormatException e) {
                            System.out.println(" Invalid input");
                        }
                        Physician phy3 = hashmap3.get(opt);
                        if (vis == 1) {
                            BookAppointment.bookVisitorAppointment(phy3, visName);
                        } else {
                            System.out.println("**************************************************");
                            System.out.println("Entered patient ID");
                            int patID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                            BookAppointment.bookAppointment(phy3, patID);
                        }
                        break;
                    case 5:
                        Map<Integer, Physician> hashmap4 = new HashMap<Integer, Physician>();
                        int count4 = 0;
                        for (Physician copy : phyList) {
                            if (Arrays.asList(copy.expertise).indexOf("Psychologist") != -1) {
                                count4++;
                                System.out.println("Press " + count4 + " for Physician: " + copy.getPhysicianName());
                                hashmap4.put(count4, copy);
                            }
                        }
                        try {
                            opt = Integer.parseInt(scan.nextLine());
                        } catch (NumberFormatException e) {
                            System.out.println(" Invalid input");
                        }
                        Physician phy4 = hashmap4.get(opt);
                        if (vis == 1) {
                            BookAppointment.bookVisitorAppointment(phy4, visName);
                        } else {
                            System.out.println("**************************************************");
                            System.out.println("Entered patient ID");
                            int patID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                            BookAppointment.bookAppointment(phy4, patID);
                        }
                        break;
                    default:
                        System.out.println("INVALID OPTION");
                        break;
                }
                break;
            case 0:
                return;
            default:
                System.out.println("INVALID OPTION");
                break;
        }

    }
}
