/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Kency Unni
 */
public class BookAppointment extends BookAppointmentData {

    public static void bookAppointment(Physician p, int pID) {
        int increment = 0;
        int i = 0;
        int opt = 0;
        int count = 0;
        boolean checkPatient = false;
        String patientName;
        BookAppointmentData.firstBook++;
        BookAppointmentData.bid++;
        if (p == null) {
            System.out.println("Invalid physician ID");
            return;
        }
        Map<Integer, BookAppointmentData> hmap = new HashMap<Integer, BookAppointmentData>();
        for (BookAppointmentData a : BookAppointmentData.appointment) {
            patientName = Patient.getPatientNameUsingPID(pID);
            if (patientName == null) {
                checkPatient = false;
            } else {
                checkPatient = true;
                break;
            }
        }
        if (checkPatient == false) {
            System.out.println("Sorry!! Entered patient is not registered in the system, Please register to book an appointment!!");
            System.out.println("Entered patient ID : " + pID);
            System.out.println("Returning to main menu");
            return;
        }
        for (BookAppointmentData a : BookAppointmentData.appointment) {
            if (a.phy_full_name == p.getPhysicianName() && a.isBooked() == false && a.isVisitor() == false) {
                increment++;
                count++;
                System.out.println("Press " + increment + " : " + a);
                hmap.put(increment, a);
            }
        }
        System.out.println("Please select the desired timeslot");
        if (count != 0) {
            try {
                opt = Integer.parseInt(BookAppointmentData.scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println(" Invalid input");
            }
            if (opt > increment || opt < 0 || opt == 0) {
                if (i != 0) {
                    System.out.println("Invalid Option");
                    System.out.println("Returning to main menu!!");
                    i = 0;
                    return;
                }
                if (i == 0) {
                    System.out.println("Invalid Option");
                    System.out.println("Please reselect the desired timeslot");
                    opt = Integer.parseInt(BookAppointmentData.scan.nextLine());
                    i++;
                }
            }
            BookAppointmentData apt = hmap.get(opt);
            for (BookAppointmentData ap : BookAppointmentData.appointment) {
                if (ap.getPatientID() == pID && ap.isBooked() == true) {
                    int result1 = ap.getStartTime().compareTo(apt.getStartTime());
                    int result2 = ap.getEndTime().compareTo(apt.getEndTime());
                    if (result1 == 0 && result2 == 0) {
                        System.out.println("Note!! You can't book this appointment, as you have already booked an appoinment in same time");
                        System.out.println("The conflicting appointment details : " + ap.toString());
                        return;
                    }
                }

            }
            int index = BookAppointmentData.appointment.indexOf(apt);
            apt.setBooked(true);
            apt.setBookingID(BookAppointmentData.bid);
            apt.setPatientID(pID);
            BookAppointmentData.appointment.set(index, apt);
            System.out.println("Please note your Booking ID  : " + apt.getBookingID());
            System.out.println("Thank you!!! Appoinment booked successfully!!!");
            int result3 = apt.getStartTime().compareTo(apt.getNewDate());
            if (result3 < 0) {
                apt.setMissed(true);
                apt.setBooked(false);
            }
        } else {
            System.out.println("All appoinments are booked");
        }
    }

    public static void attend(int patID) {
        int index;
        int increment = 0;
        int i = 0;
        int opt = 0;
        int count = 0;
        Physician phys = new Physician();
        if (patID == 0) {
            System.out.println("Invalid patient ID- Patient ID can't be Zero");
            return;
        }
        if (BookAppointmentData.firstBook == 0) {
            System.out.println("Sorry!! You have not booked any appointments yet!! ");
            return;
        }
        System.out.println("Please select the appointment you wish to change");
        Map<Integer, BookAppointmentData> hmap = new HashMap<Integer, BookAppointmentData>();
        for (BookAppointmentData p : BookAppointmentData.appointment) {
            if (p.getPatientID() == patID && p.isBooked() == true && p.isAttended() == false) {
                increment++;
                count++;
                System.out.println("Press " + increment + " : " + p);
                hmap.put(increment, p);
            }
        }
        if (count != 0) {
            try {
                opt = Integer.parseInt(BookAppointmentData.scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println(" Invalid input");
            }
            if (opt > increment || opt < 0) {
                System.out.println("Invalid Option");
                return;
            }
            BookAppointmentData apt = hmap.get(opt);
            int index1 = BookAppointmentData.appointment.indexOf(apt);
            apt.setAttended(true);
            BookAppointmentData.appointment.set(index1, apt);
        } else {
            System.out.println("Sorry!!You didn't booked any appointments");
            return;
        }
        System.out.println(" Thank you for attending ");
    }

    public static void changeAppointment() throws IOException {
        int vis = 0;
        int choice1 = 0;
        if (BookAppointmentData.firstBook == 0) {
            System.out.println("Sorry!! You have not booked any appointments yet!! ");
            return;
        }
        System.out.println("**************************************************");
        System.out.println("Change an booked appoinment");
        System.out.println("Press 1 to change appointment using booking ID");
        System.out.println("Press 2 to change appointment using patient ID");
        try {
            choice1 = Integer.parseInt(BookAppointmentData.scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println(" Invalid input");
        }
        System.out.println("**************************************************");
        switch (choice1) {
            case 1:
                int index = 0;
                System.out.println("Please enter your booking id you want to change");
                int bookID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                for (BookAppointmentData p : BookAppointmentData.appointment) {
                    if (p.getBookingID() == bookID && p.isBooked() == true) {
                        if (bookID == 0) {
                            System.out.println("Invalid booking ID- booking ID can't be Zero");
                            return;
                        }
                        index = BookAppointmentData.appointment.indexOf(p);
                        p.setBooked(false);
                        p.setChanged(true);
                        BookAppointmentData.appointment.set(index, p);
                        if (p.isVisitor() == true) {
                            vis = 1;
                        }
                    }
                }
                break;
            case 2:
                System.out.println("Please enter your patient id");
                int ppID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                int increment = 0;
                int count = 0;
                int opt = 0;
                System.out.println("Please select the appointment you wish to change");
                Map<Integer, BookAppointmentData> hmap = new HashMap<Integer, BookAppointmentData>();
                for (BookAppointmentData p : BookAppointmentData.appointment) {
                    if (p.getPatientID() == ppID && p.isBooked() == true) {
                        increment++;
                        count++;
                        System.out.println("Press " + increment + " : " + p);
                        hmap.put(increment, p);
                    }
                }
                if (count != 0) {
                    opt = Integer.parseInt(BookAppointmentData.scan.nextLine());
                    if (opt > increment || opt < 0) {
                        System.out.println("Invalid Option");
                        return;
                    }
                    BookAppointmentData apt = hmap.get(opt);
                    int index1 = BookAppointmentData.appointment.indexOf(apt);
                    apt.setBooked(false);
                    apt.setChanged(true);
                    apt.setBookingID(600);
                    BookAppointmentData.appointment.set(index1, apt);
                } else {
                    System.out.println("Sorry!!You didn't booked any appointments");
                    return;
                }
                break;
            case 0:
                return;
            default:
                System.out.println("INVALID OPTION");
                System.out.println("Returning to main menu!!");
                return;
        }
        System.out.println("Please book your new/changed appoinment ");
        if (vis == 1) {
            BookAppointmentData.phy.physicianLookUp(1);
        } else {
            BookAppointmentData.phy.physicianLookUp(0);
        }
        System.out.println("Booking changed successfully!!!");
    }

    public static void bookVisitorAppointment(Physician p, String visName) {
        if (p == null) {
            System.out.println("Invalid Physician ID");
            return;
        }
        if (visName == null) {
            System.out.println("Visitor name cannot be NULL");
            return;
        }
        int increment = 0;
        int i = 0;
        int opt = 0;
        int count = 0;
        BookAppointmentData.firstBook++;
        BookAppointmentData.bid++;
        Map<Integer, BookAppointmentData> hmap = new HashMap<Integer, BookAppointmentData>();
        for (BookAppointmentData a : BookAppointmentData.appointment) {
            if (a.phy_full_name == p.getPhysicianName() && a.isBooked() == false && a.isVisitor() == true) {
                increment++;
                count++;
                System.out.println("Press " + increment + " : " + a);
                hmap.put(increment, a);
            }
        }
        if (count != 0) {
            System.out.println("Please select the desired timeslot");
            try {
                opt = Integer.parseInt(BookAppointmentData.scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println(" Invalid input");
            }
            if (opt > increment || opt < 0 || opt == 0) {
                if (i != 0) {
                    System.out.println("Invalid Option");
                    System.out.println("Returning to main menu!!");
                    i = 0;
                    return;
                }
                if (i == 0) {
                    System.out.println("Invalid Option");
                    System.out.println("Please reselect the desired timeslot");
                    opt = Integer.parseInt(BookAppointmentData.scan.nextLine());
                    i++;
                }
            }
            BookAppointmentData apt = hmap.get(opt);
            int index = BookAppointmentData.appointment.indexOf(apt);
            apt.setBooked(true);
            apt.setAttended(true);
            apt.setVisitorName(visName);
            apt.setBookingID(BookAppointmentData.bid);
            BookAppointmentData.appointment.set(index, apt);
            System.out.println("Please note your Booking ID  : " + apt.getBookingID());
            System.out.println("Thank you!!! Appoinment booked successfully!!!");
        } else {
            System.out.println("All appoinments are booked");
        }
    }

    public static void cancelAppointment() {
        int index;
        int choice1 = 0;
        //        physicianLookUp phys = new physicianLookUp();
        if (BookAppointmentData.firstBook == 0) {
            System.out.println("Sorry!! You have not booked any appointments yet!! ");
            return;
        }
        System.out.println("**************************************************");
        System.out.println("Cancel an booked appoinment");
        System.out.println("Press 1 to cancel appointment using booking ID");
        System.out.println("Press 2 to cancel appointment using patient ID");
        try {
            choice1 = Integer.parseInt(BookAppointmentData.scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println(" Invalid input");
        }
        System.out.println("**************************************************");
        if (BookAppointmentData.firstBook == 0) {
            System.out.println("Sorry!! You have not booked any appointments yet!! ");
            return;
        }
        System.out.println("**************************************************");
        switch (choice1) {
            case 1:
                int bookID = 0;
                System.out.println("Please enter your booking id you want to cancel");
                try {
                    bookID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                } catch (NumberFormatException e) {
                    System.out.println(" Invalid input");
                }
                for (BookAppointmentData p : BookAppointmentData.appointment) {
                    if (p.getBookingID() == bookID && p.isBooked() == true) {
                        if (bookID == 0) {
                            System.out.println("Invalid booking ID- booking ID can't be Zero");
                            return;
                        }
                        index = BookAppointmentData.appointment.indexOf(p);
                        p.setBooked(false);
                        p.setCancelled(true);
                        BookAppointmentData.firstBook--;
                        BookAppointmentData.appointment.set(index, p);
                        System.out.println("Thank you!! Appointment Cancelled!!");
                    }
                }
                break;
            case 2:
                int ppID = 0;
                System.out.println("Please enter your patient id");
                try {
                    ppID = Integer.parseInt(BookAppointmentData.scan.nextLine());
                } catch (NumberFormatException e) {
                    System.out.println(" Invalid input");
                }
                int increment = 0;
                int count = 0;
                int opt = 0;
                System.out.println("Please select the appointment you wish to cancel");
                Map<Integer, BookAppointmentData> hmap = new HashMap<Integer, BookAppointmentData>();
                for (BookAppointmentData p : BookAppointmentData.appointment) {
                    if (p.getPatientID() == ppID && p.isBooked() == true) {
                        increment++;
                        count++;
                        System.out.println("Press " + increment + " : " + p);
                        hmap.put(increment, p);
                    }
                }
                if (count != 0) {
                    try {
                        opt = Integer.parseInt(BookAppointmentData.scan.nextLine());
                    } catch (NumberFormatException e) {
                        System.out.println(" Invalid input");
                    }
                    if (opt > increment || opt < 0) {
                        System.out.println("Invalid Option");
                        return;
                    }
                    BookAppointmentData apt = hmap.get(opt);
                    int index1 = BookAppointmentData.appointment.indexOf(apt);
                    apt.setBooked(false);
                    apt.setCancelled(true);
                    BookAppointmentData.appointment.set(index1, apt);
                    System.out.println("Thank you!! Appointment Cancelled!!");
                } else {
                    System.out.println("Sorry!!You didn't booked any appointments");
                }
                break;
            case 0:
                return;
            default:
                System.out.println("INVALID OPTION");
                break;
        }
    }

}
