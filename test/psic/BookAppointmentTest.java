/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.io.InputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Kency Unni
 */
public class BookAppointmentTest {

    public BookAppointmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of bookAppointment method, of class BookAppointment.
     */
    @Test
    public void testBookAppointmentNull() {
        System.out.println("BookAppointmentNull");
        Physician p = null;
        BookAppointment.bookAppointment(p, 1);
    }

//    @Test
//    public void testBookAppointment() {
//        System.out.println("BookAppointment");
//        System.out.println("..................................");
//        ReportTest rt = new ReportTest();
//        BookAppointmentData.addAppointment();
//        Patient pat = new Patient();
//        pat.preRegisteredPatient();
//        Physician p = new Physician(3, "Dr. Lekshmi", "LU2 7GB", "07737389892", new String[]{"Physiotherapy", "Rehabilitation", "Osteopathy"});
//        BookAppointment.bookAppointment(p, 121);
//        rt.testReport1();
//
//    }

    /**
     * Test of attend method, of class BookAppointment.
     */
    @Test
    public void testAttendPatientIDZero() {
        System.out.println("AttendPatientIDZero");
        int patID = 0;
        BookAppointment.attend(patID);
    }

    @Test
    public void testAttend() {
        System.out.println("Attend");
        ReportTest rt = new ReportTest();
        BookAppointment.attend(121);
        rt.testReport1();
    }

    /**
     * Test of changeAppointment method, of class BookAppointment.
     *
     * @throws java.lang.Exception
     */
//    @Test
//    public void testChangeAppointment() throws Exception {
//        System.out.println("changeAppointment");
//        InputStream backup = System.in;
//        //ByteArrayInputStream in = new ByteArrayInputStream("My String   ".getBytes("1"));
//        System.setIn(System.in);
//        BookAppointment.changeAppointment();
//        System.setIn(backup);
//    }

    /**
     * Test of bookVisitorAppointment method, of class BookAppointment.
     */
    @Test
    public void testBookVisitorAppointmentNull() {
        System.out.println("bookVisitorAppointmentNull");
        Physician p = null;
        String visName = "";
        BookAppointment.bookVisitorAppointment(p, visName);
    }

//    @Test
//    public void testBookVisitorAppointment() {
//        System.out.println("bookVisitorAppointment");
//        ReportTest rt = new ReportTest();
//        Physician p = new Physician(3, "Dr. Lekshmi", "LU2 7GB", "07737389892", new String[]{"Physiotherapy", "Rehabilitation", "Osteopathy"});
//        BookAppointmentData.addAppointment();
//        String visName = "test";
//        BookAppointment.bookVisitorAppointment(p, visName);
//        rt.testReport1();
//    }

    /**
     * Test of cancelAppointment method, of class BookAppointment.
     */
//    @Test
//    public void testCancelAppointment() {
//        System.out.println("cancelAppointment");
//        BookAppointment.cancelAppointment();
//    }

}
