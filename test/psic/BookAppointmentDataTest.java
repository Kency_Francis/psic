/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static psic.BookAppointmentData.dateGenerator;

/**
 *
 * @author Kency Unni
 */
public class BookAppointmentDataTest {

    public BookAppointmentDataTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getVisitorName method, of class BookAppointmentData.
     */
    @Test
    public void testGetVisitorName() {
        System.out.println("getVisitorName");
        BookAppointmentData instance = new BookAppointmentData();
        String expResult = "kens";
        instance.setVisitorName(expResult);
        String result = instance.getVisitorName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVisitorName method, of class BookAppointmentData.
     */
    @Test
    public void testSetVisitorName() {
        System.out.println("setVisitorName");
        String visitorName = "unni";
        BookAppointmentData instance = new BookAppointmentData();
        instance.setVisitorName(visitorName);
        String result = instance.getVisitorName();
        assertEquals(visitorName, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        String expResult1 = "\n" + "Physician Name:Dr. Francis  Physician ID:1  Start time :Fri Apr 02 14:30:00 BST 2021  End time :Fri Apr 02 14:40:00 BST 2021  Treatment : Pain Management               Room  :Medical consulting suite A";
        String result1 = obj.toString();
        System.out.println(result1);
        System.out.println(expResult1);
        assertEquals(expResult1, result1);

    }

    /**
     * Test of isBooked method, of class BookAppointmentData.
     */
    @Test
    public void testIsBooked() {
        System.out.println("isBooked");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        boolean expResult = false;
        boolean result = obj.isBooked();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBooked method, of class BookAppointmentData.
     */
    @Test
    public void testSetBooked() {
        System.out.println("setBooked");
        boolean booked = false;
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        obj.setBooked(booked);
        boolean result = obj.isBooked();
        assertEquals(result, booked);
    }

    /**
     * Test of isAttended method, of class BookAppointmentData.
     */
    @Test
    public void testIsAttended() {
        System.out.println("isAttended");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        boolean expResult = false;
        boolean result = obj.isAttended();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAttended method, of class BookAppointmentData.
     */
    @Test
    public void testSetAttended() {
        System.out.println("setAttended");
        boolean attended = false;
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        obj.setAttended(attended);
        boolean result = obj.isAttended();
        assertEquals(result, attended);
    }

    /**
     * Test of isChanged method, of class BookAppointmentData.
     */
    @Test
    public void testIsChanged() {
        System.out.println("isChanged");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        boolean expResult = false;
        boolean result = obj.isChanged();
        assertEquals(expResult, result);
    }

    /**
     * Test of setChanged method, of class BookAppointmentData.
     */
    @Test
    public void testSetChanged() {
        System.out.println("setChanged");
        boolean changed = false;
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        obj.setChanged(changed);
        boolean result = obj.isChanged();
        assertEquals(result, changed);
    }

    /**
     * Test of isCancelled method, of class BookAppointmentData.
     */
    @Test
    public void testIsCancelled() {
        System.out.println("isCancelled");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        boolean expResult = false;
        boolean result = obj.isCancelled();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCancelled method, of class BookAppointmentData.
     */
    @Test
    public void testSetCancelled() {
        System.out.println("setCancelled");
        boolean cancelled = false;
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        obj.setCancelled(cancelled);
        boolean result = obj.isCancelled();
        assertEquals(result, cancelled);
    }

    /**
     * Test of isVisitor method, of class BookAppointmentData.
     */
    @Test
    public void testIsVisitor() {
        System.out.println("isVisitor");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        boolean expResult = false;
        boolean result = obj.isVisitor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPatientID method, of class BookAppointmentData.
     */
    @Test
    public void testGetPatientID() {
        System.out.println("getPatientID");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        int expResult = 100;
        obj.setPatientID(expResult);
        int result = obj.getPatientID();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPatientID method, of class BookAppointmentData.
     */
    @Test
    public void testSetPatientID() {
        System.out.println("setPatientID");
        int patientID = 150;
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        obj.setPatientID(patientID);
        int result = obj.getPatientID();
        assertEquals(patientID, result);
    }

    /**
     * Test of getBookingID method, of class BookAppointmentData.
     */
    @Test
    public void testGetBookingID() {
        System.out.println("getBookingID");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        int expResult = 120;
        obj.setBookingID(expResult);
        int result = obj.getBookingID();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBookingID method, of class BookAppointmentData.
     */
    @Test
    public void testSetBookingID() {
        System.out.println("setBookingID");
        int bookingID = 140;
        BookAppointmentData instance = new BookAppointmentData();
        instance.setBookingID(bookingID);
        int result = instance.getBookingID();
        assertEquals(bookingID, result);
    }

    /**
     * Test of getStartTime method, of class BookAppointmentData.
     */
    @Test
    public void testGetStartTime() {
        System.out.println("getStartTime");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        Date expResult = dateGenerator("02-04-2021 14:30:00");
        Date result = obj.getStartTime();
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of setStartTime method, of class BookAppointmentData.
     */
    @Test
    public void testSetStartTime() {
        System.out.println("setStartTime");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        Date startTime = dateGenerator("02-04-2021 14:30:00");
        obj.setStartTime(startTime);
        System.out.println(startTime);
        System.out.println(obj.getStartTime());
        assertEquals(startTime, obj.getStartTime());
    }

    /**
     * Test of getEndTime method, of class BookAppointmentData.
     */
    @Test
    public void testGetEndTime() {
        System.out.println("getEndTime");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        Date expResult = dateGenerator("02-04-2021 14:40:00");
        Date result = obj.getEndTime();
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of setEndTime method, of class BookAppointmentData.
     */
    @Test
    public void testSetEndTime() {
        System.out.println("setEndTime");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        Date endTime = dateGenerator("02-04-2021 14:30:00");
        obj.setEndTime(endTime);
        System.out.println(endTime);
        System.out.println(obj.getStartTime());
        assertEquals(endTime, obj.getStartTime());
    }

    /**
     * Test of setVisitor method, of class BookAppointmentData.
     */
    @Test
    public void testSetVisitor() {
        System.out.println("setVisitor");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1, true);
        boolean Visitor = true;
        obj.setVisitor(Visitor);
        assertEquals(Visitor, obj.isVisitor());
    }

    /**
     * Test of addAppointment method, of class BookAppointmentData.
     */
    @Test
    public void testAddAppointment() {
        System.out.println("addAppointment");
        BookAppointmentData.addAppointment();
    }

    /**
     * Test of isMissed method, of class BookAppointmentData.
     */
    @Test
    public void testIsMissed() {
        System.out.println("isMissed");
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        boolean expResult = false;
        boolean result = obj.isMissed();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMissed method, of class BookAppointmentData.
     */
    @Test
    public void testSetMissed() {
        System.out.println("setMissed");
        boolean missed = false;
        BookAppointmentData obj = new BookAppointmentData(dateGenerator("02-04-2021 14:30:00"), dateGenerator("02-04-2021 14:40:00"), "Pain Management               ", "Medical consulting suite A", "Dr. Francis", 1);
        obj.setCancelled(missed);
        boolean result = obj.isMissed();
        assertEquals(result, missed);
    }

}
