/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kency Unni
 */
public class PhysicianTest {

    public PhysicianTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        Physician.addPhysician();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class physicianLookUp.
     */
    @Test
    public void testToString() {
        System.out.println("testToString");
        Physician phy = new Physician(2, "Dr. Tester", "4 Althrop Road", "07126678324", new String[]{"Osteopathy"});
        String expResult1 = "2 Dr. Tester 4 Althrop Road 07126678324 [Osteopathy]";
        String result1 = phy.toString();
        System.out.println(expResult1);
        System.out.println(result1);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of getPhysicianName method, of class physicianLookUp.
     */
    @Test
    public void testGetPhysicianName() {
        System.out.println("getPhysicianName");
        String expResult = "Dr. Tester";
        Physician phy = new Physician(2, "Dr. Tester", "4 Althrop Road", "07126678324", new String[]{"Osteopathy"});
        String result = phy.getPhysicianName();
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);

    }

    /**
     * Test of getUnique_id method, of class physicianLookUp.
     */
    @Test
    public void testGetUnique_id() {
        System.out.println("getUnique_id");
        int expResult = 2;
        Physician phy = new Physician(2, "Dr. Tester", "4 Althrop Road", "07126678324", new String[]{"Osteopathy"});
        int result = phy.getUnique_id();
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);

    }

    /**
     * Test of setUnique_id method, of class physicianLookUp.
     */
    @Test
    public void testSetUnique_id() {
        System.out.println("setUnique_id");
        int expResult = 21;
        Physician phy = new Physician(2, "Dr. Tester", "4 Althrop Road", "07126678324", new String[]{"Osteopathy"});
        phy.setUnique_id(expResult);
        System.out.println(expResult);
        System.out.println(phy.getUnique_id());
        assertEquals(expResult, phy.getUnique_id());
    }

    /**
     * Test of getPhysicianExpertise method, of class physicianLookUp.
     */
    @Test
    public void testGetPhysicianExpertise() {
        System.out.println("getPhysicianExpertise");
        String[] expResult = new String[]{"Osteopathy"};
        Physician phy = new Physician(2, "Dr. Tester", "4 Althrop Road", "07126678324", new String[]{"Osteopathy"});
        String[] result = phy.getPhysicianExpertise();
        System.out.println(Arrays.toString(result));
        System.out.println(Arrays.toString(expResult));
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of listAllPhysicians method, of class physicianLookUp.
     */
    @Test
    public void testListAllPhysicians() {
        System.out.println("listAllPhysicians");
        Physician phy = new Physician(2, "Dr. Tester", "4 Althrop Road", "07126678324", new String[]{"Osteopathy"});
        phy.listAllPhysicians();
    }

    /**
     * Test of addPhysician method, of class physicianLookUp.
     */
    @Test
    public void testAddPhysician() {
        System.out.println("addPhysician");
        Physician.addPhysician();
    }

}
