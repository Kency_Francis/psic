/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kency Unni
 */
public class ReportTest {
    
    public ReportTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of report1 method, of class Report.
     */
    @Test
    public void testReport1() {
        System.out.println("report1");
        Report.report1();
    }

    /**
     * Test of reportt2 method, of class Report.
     */
    @Test
    public void testReportt2() {
        System.out.println("reportt2");
        Report.reportt2();
    }

    /**
     * Test of reportALL method, of class Report.
     */
    @Test
    public void testReportALL() {
        System.out.println("reportALL");
        Report.reportALL();
    }
    
}
