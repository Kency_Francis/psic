/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kency Unni
 */
public class PatientTest {

    public PatientTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of printPatient method, of class Patient.
     */
    /**
     * Test of getUnique_id method, of class Patient.
     */
    @Test
    public void testGetUnique_id() {
        System.out.println("getUnique_id");
        int expResult = 999;
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        int result = instance.getUnique_id();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPatientName method, of class Patient.
     */
    @Test
    public void testGetPatientName() {
        System.out.println("getPatientName");
        String expResult = "Test";
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        String result = instance.getPatientName();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Patient.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "999   Test   3 Althrop Road   7346273938";
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        String result = instance.toString();
        System.out.println("ID" + "   " + "Patient Name" + "   " + "Address" + "   " + "Phone Number");
        System.out.println(expResult);
        System.out.println("ID" + "   " + "Patient Name" + "   " + "Address" + "   " + "Phone Number");
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of preRegisteredPatient method, of class Patient.
     */
    @Test
    public void testPreRegisteredPatient() {
        System.out.println("preRegisteredPatient");
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        instance.preRegisteredPatient();
    }

    /**
     * Test of listAllPatient method, of class Patient.
     */
    @Test
    public void testListAllPatient() {
        System.out.println("listAllPatient");
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        instance.listAllPatient();
    }

    /**
     * Test of getPatientNameUsingPID method, of class Patient.
     */
    @Test
    public void testGetPatientNameUsingPID() {
        System.out.println("getPatientNameUsingPID");
        int patID = 121;
        String expResult = "Kency Francis   ";
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        instance.preRegisteredPatient();
        String result = Patient.getPatientNameUsingPID(patID);
        System.out.println(result);
        System.out.println(expResult);
        assertEquals(expResult, result);
    }

    /**
     * Test of setUnique_id method, of class Patient.
     */
    @Test
    public void testSetUnique_id() {
        System.out.println("setUnique_id");
        int unique_id = 120;
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        instance.setUnique_id(unique_id);
        System.out.println(unique_id);
        System.out.println(instance.getUnique_id());
        assertEquals(unique_id, instance.getUnique_id());
    }

    /**
     * Test of listPatientUsingPID method, of class Patient.
     */
    @Test
    public void testListPatientUsingPID() {
        System.out.println("listPatientUsingPID");
        Patient instance = new Patient(999, "Test", "3 Althrop Road", "7346273938");
        instance.listPatientUsingPID(333);
    }

    /**
     * Test of getPIDNameUsingPname method, of class Patient.
     */
    @Test
    public void testGetPIDNameUsingPname() {
        System.out.println("getPIDNameUsingPname");
        String pname = "Lilly Shyni     ";
        int expResult = 323;
        int result = Patient.getPIDNameUsingPname(pname);
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);
    }

}
